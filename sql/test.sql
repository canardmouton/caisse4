DROP TABLE IF EXISTS "public"."product" CASCADE;

CREATE TABLE public.product (
      id INTEGER PRIMARY KEY NOT NULL DEFAULT nextval('product_id_seq'::regclass),
      ref CHARACTER VARYING(30) NOT NULL,
      price DOUBLE PRECISION NOT NULL
);



