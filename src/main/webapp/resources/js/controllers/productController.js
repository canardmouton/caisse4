/**
 * Created by nea on 30/09/16.
 */
'use strict';

/**
 * ProductController
 * @constructor
 */
App.controller("productController", function($scope, $http) {
    $http.get("/CA/api/getIp").success(function(data) {
        console.log('dataip = ', data.appip);
        $scope.model = data;
    });

    $scope.callHandshake = function () {
        $http.get("/CA/api/handshake").success(function(data) {
            console.log(data);
        });
    };

    $scope.getMessageGet = function () {
        $http.get("/CA/testMsg").success(function(data) {
            console.log(data);
        });
    };

    $scope.getMessagePost = function () {
        $http.post("/CA/testService").success(function(data) {
            console.log(data);
        });
    };

    $scope.getSendFile = function () {
        $http.post("/CA/testFile").success(function(data) {
            console.log(data);
        });
    };

    $scope.checkFile = function() {
        $http.post("/CA/checkFile").success(function(data) {
            console.log(data);
        });
    };
});