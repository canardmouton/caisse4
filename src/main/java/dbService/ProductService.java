package dbService;

import Service.CaisseService.ReceiptProduct;
import Service.CaisseService.ReferenceProduct;
import model.ProductEntity;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by patrickear on 21/1/2017.
 */
public class ProductService {
    private EntityManagerFactory emf;
    private EntityManager em;

    public ProductService() {
        this.emf = Persistence.createEntityManagerFactory("caisse4");
        this.em = emf.createEntityManager();
    }

    public void createProduct(ProductEntity productEntity) {
        em.getTransaction().begin();
        em.persist(productEntity);
        em.getTransaction().commit();
    }

    public void createListOfProducts(List<ReferenceProduct> list) {
        System.out.println("== createListOfProducts ==");
        em.getTransaction().begin();

        for (ReferenceProduct referenceProduct: list) {
            em.persist(new ProductEntity(referenceProduct.getProductRef(), referenceProduct.getPrice()));
        }

        em.getTransaction().commit();
    }

    private List<ProductEntity> getAllProduct() {
        System.out.println("== getAllProduct ==");
        Query query = em.createQuery("SELECT e FROM ProductEntity e WHERE 1 = 1");
        List<ProductEntity> productEntities = query.getResultList();
        return productEntities;
    }

    public ProductEntity getProductByRef(String ref) {
        ProductEntity productEntity = (ProductEntity) em.createQuery("SELECT e FROM ProductEntity e WHERE e.ref = :value1")
                .setParameter("value1", ref).getSingleResult();
        return productEntity;
    }

    public void removeAllProduct() {
        System.out.println("== removeAllProduct ==");
        List<ProductEntity> productEntities = this.getAllProduct();
        em.getTransaction().begin();
        for (ProductEntity productEntity: productEntities) {
            System.out.println("Id = " + productEntity.getId());
            System.out.println("Ref = " + productEntity.getRef());
            em.remove(productEntity);
        }
        em.getTransaction().commit();
    }
}
