package model;

import javax.persistence.*;

/**
 * Created by patrickear on 27/1/2017.
 */
@Entity
@Table(name = "promotion", schema = "public", catalog = "caisse4")
public class PromotionEntity {
    @Id
    @SequenceGenerator(name="promotion_id_seq",sequenceName="promotion_id_seq", allocationSize=1)
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator="promotion_id_seq")
    @Column(name = "id", nullable = false)
    private int id;

    private String productref;
    private double valeur;

    public PromotionEntity(String productref, double valeur) {
        this.productref = productref;
        this.valeur = valeur;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "productref", nullable = true, length = 10)
    public String getProductref() {
        return productref;
    }

    public void setProductref(String productref) {
        this.productref = productref;
    }

    @Basic
    @Column(name = "valeur", nullable = false, precision = 0)
    public double getValeur() {
        return valeur;
    }

    public void setValeur(double valeur) {
        this.valeur = valeur;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PromotionEntity that = (PromotionEntity) o;

        if (id != that.id) return false;
        if (Double.compare(that.valeur, valeur) != 0) return false;
        if (productref != null ? !productref.equals(that.productref) : that.productref != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = id;
        result = 31 * result + (productref != null ? productref.hashCode() : 0);
        temp = Double.doubleToLongBits(valeur);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
