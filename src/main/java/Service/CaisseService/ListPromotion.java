package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by patrickear on 22/1/2017.
 */
public class ListPromotion implements CaisseJson {
    List<Promotion> list_promotion;

    public ListPromotion(List<Promotion> list_promotion) {
        this.list_promotion = list_promotion;
    }

    public List<Promotion> getList_promotion() { return list_promotion; }

    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
