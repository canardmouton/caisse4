package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by patrickear on 14/1/2017.
 */
public class Customer implements CaisseJson {
    private String modePaiement;
    private String carteFid;
    private String card;
    private List<Produit> panier;

    public Customer(String modePaiement, String carteFid, String card, List<Produit> panier) {
        this.modePaiement = modePaiement;
        this.carteFid = carteFid;
        this.card = card;
        this.panier = panier;
    }

    public String getModePaiement() { return modePaiement; }

    public String getCarteFid() { return carteFid; }

    public String getCard() { return card; }

    public List<Produit> getPanier() {
        return panier;
    }

    public boolean isValid() {
        if (modePaiement == null)
            return false;

        for (Produit produit: panier) {
            if (produit.getCodeProduit() == null || produit.getQuantity() <= 0)
                return false;
        }

        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
