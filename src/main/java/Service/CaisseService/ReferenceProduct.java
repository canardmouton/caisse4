package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by patrickear on 20/1/2017.
 */
public class ReferenceProduct {
    private String productRef;
    private double price;

    public ReferenceProduct(String productRef, double price) {
        this.productRef = productRef;
        this.price = price;
    }

    public String getProductRef() { return productRef; }

    public double getPrice() { return price; }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
