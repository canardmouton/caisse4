package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.List;

/**
 * Created by patrickear on 22/1/2017.
 */
public class ListCoupon implements CaisseJson {
    String refClient;
    List<Coupon> listCoupons;

    public ListCoupon(String refClient, List<Coupon> listCoupon) {
        this.refClient = refClient;
        this.listCoupons = listCoupon;
    }

    public String getReceiptId() { return refClient; }

    public List<Coupon> getListCoupon() { return listCoupons; }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
