package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by patrickear on 16/1/2017.
 */
public class Promotion {
    private String productRef;
    private double valeur;

    public Promotion(String productRef, double valeur) {
        this.productRef = productRef;
        this.valeur = valeur;
    }

    public String getProductRef() { return productRef; }

    public double getValeur() { return valeur; }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
