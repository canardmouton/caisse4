package Service.CaisseService;

import org.apache.commons.lang.builder.ToStringBuilder;

/**
 * Created by patrickear on 20/1/2017.
 */
public class Receipt implements CaisseJson {
    private String receiptId;
    private String fidelityCard; // fidelity
    private String creditCard;
    private double amount;

    public Receipt(String receiptId, String fidelityCard, String creditCard, double amount) {
        this.receiptId = receiptId;
        this.fidelityCard = fidelityCard;
        this.creditCard = creditCard;
        this.amount = amount;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}