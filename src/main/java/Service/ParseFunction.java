package Service;

import Service.CaisseService.*;
import Service.ServicesKit.MyHttpPost;
import com.google.gson.Gson;
import dbService.ProductService;
import dbService.PromotionService;
import model.ProductEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class ParseFunction {
    private String appName;
    private int instanceID;
    private String appip;
    private String stip;

    private String fct = "";
    private String data = "";
    private Gson gson = new Gson();

    private ProductService productService;
    private PromotionService promotionService;

    public ParseFunction(String fct, String data) {
        this.fct = fct;
        this.data = data;
        this.productService = new ProductService();
        this.promotionService = new PromotionService();
    }

    public ParseFunction(String appName, int instanceID, String appip, String stip) {
        this.appName = appName;
        this.instanceID = instanceID;
        this.appip = appip;
        this.stip = stip;
        this.productService = new ProductService();
        this.promotionService = new PromotionService();
    }

    public void setFct(String fct) {
        this.fct = fct;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String execute() {
        switch (fct) {
            case "ticket": { // UT-CA-1
                System.out.println("Inside ticket");
                System.out.println("data = " + data);

                CustomerData customerData = gson.fromJson(data, CustomerData.class);
                Customer customer = customerData.getData();
                List<Produit> panier = customer.getPanier();

                System.out.println(panier.toString());

                if (!customer.isValid()) {
                    return "KO";
                } else {
                    List<ReceiptProduct> basket = panier.stream()
                            .map(produit -> new ReceiptProduct(produit.getCodeProduit(), produit.getQuantity()))
                            .collect(Collectors.toList());

                    String carteFid = customer.getCarteFid();

                    double total = basket.stream()
                            .map(product -> {
                                ProductEntity productEntity = productService.getProductByRef(product.getProductRef());
                                return productEntity.getPrice() * product.getQuantity();
                            })
                            .reduce(0d, (a, b) -> a + b);

                    PurchaseInfo purchaseInfo = new PurchaseInfo("0", carteFid, total, basket); // FIXME need to set purchase data receiptId & customer id
                    CaisseJsonHeader boDataObject = new CaisseJsonHeader("CA", this.instanceID, purchaseInfo);
                    String boData = new Gson().toJson(boDataObject);

                    System.out.println("Data Send to BO = " + boData);

                    String boInstanceId = "1";
                    if (this.instanceID == 3 || this.instanceID == 4) {
                        boInstanceId = "2";
                    }

                    // Send ticket to BO
                    sendMsg("ticketToBO", "BO", boInstanceId, boData); // asynchrone

                    if (customer.getModePaiement().equals("CARD")) { // CA-MO-1
                        System.out.println("Customer is paying " + total + " € by card with card no : " + customer.getCard());
                        BankCardPayment bankCardPayment = new BankCardPayment(customer.getCard(), total);
                        CaisseJsonHeader moDataObject = new CaisseJsonHeader("CA", this.instanceID, bankCardPayment);
                        String moData = new Gson().toJson(moDataObject);

                        System.out.println("Data Send to MO =" + moData);
//                        sendWebService("ticketReceiptToMO", "CA", "1", moData); // synchrone
                        String moResult = sendWebService("ticketReceiptToMO", "MO", "1", moData); // synchrone
                        System.out.println("Payement by card Monetique result " + moResult);
                    } else if (customer.getModePaiement().equals("DIFFERED")) { // CA-MO-2
                        System.out.println("Customer differed his payment of " + total + " € with the fidelity card no :" + customer.getCarteFid());
                        FidelityCardPayment fidelityCardPayment = new FidelityCardPayment(customer.getCarteFid(), total);
                        CaisseJsonHeader moDataObject = new CaisseJsonHeader("CA", this.instanceID, fidelityCardPayment);
                        String moData = new Gson().toJson(moDataObject);

                        System.out.println("Data Send to MO = " + moData);
//                        sendWebService("ticketReceiptToMO", "CA", "1", moData); // synchrone
                        String moResult = sendWebService("ticketReceiptFidToMO", "MO", "1", moData); // synchrone
                        System.out.println("Differed payment Monetique result " + moResult);
                    } else if (customer.getModePaiement().equals("CASH")) { // CA-MO-2
                        System.out.println("Customer is paying " + total + "€ by cash");
                    }

                    return "OK";
                }
            }
            case "promotionsToCA": { // BO-CA-1
                System.out.println("Inside promotionsToCaisse");
                System.out.println("data = " + data);

                PromotionData promotionData = gson.fromJson(data, PromotionData.class);
                ListPromotion listPromotion = promotionData.getData();
                List<Promotion> promotionList = listPromotion.getList_promotion();

                promotionService.createListOfProducts(promotionList);

                System.out.println(promotionData.toString());
                System.out.println(listPromotion.toString());
                return "KO";
            }
            case "productsToCA": { // BO-CA-2
                System.out.println("Inside productsToCaisse");
                System.out.println("data = " + data);

                ProductReferenceData productReferenceData = gson.fromJson(data, ProductReferenceData.class);
                ListReferenceProduct listReferenceProduct = productReferenceData.getData();

                List<ReferenceProduct> referenceProductList = listReferenceProduct.getListProduct();

                productService.removeAllProduct();
                productService.createListOfProducts(referenceProductList);

                System.out.println(productReferenceData.toString());
                System.out.println(listReferenceProduct.toString());
                return "OK";
            }
            case "couponsToCA": { // BO-CA-3
                System.out.println("Inside couponsToCaisse");
                System.out.println("data = " + data);

                CouponData couponData = gson.fromJson(data, CouponData.class);
                ListCoupon coupons = couponData.getData();
                List<Coupon> couponList = coupons.getListCoupon();

                System.out.println(couponData.toString());
                System.out.println("List of coupon of the purchase : " + coupons.getReceiptId());
                for (Coupon coupon: couponList) {
                    System.out.println("You received couponType : " + coupon.getType_produit()
                            + " that allow you to have some discount : " + coupon.getDiscount());
                }
                return "KO";
            }

            // UNIT TEST
            case "ticketToBO": {
                System.out.println("Inside ticketToBO");
                System.out.println("data = " + data);
                return "UNIT TEST ticketToBO OK";
            }

            case "ticketReceiptToMO": {
                System.out.println("Inside ticketReceiptToMO");
                System.out.println("data = " + data);
                return "UNIT TEST ticketReceiptToMO OK";
            }

            case "ticketReceiptFidToMO": {
                System.out.println("Inside ticketReceiptToMO");
                System.out.println("data = " + data);
                return "UNIT TEST ticketReceiptFidToMO OK";
            }

            // TEST IS HERE
            case "test_ticketReceiptToMO": {
                Receipt receipt = new Receipt("RECEIPT", "FID-4242", "CREDIT-2446", 42.42);
                CaisseJsonHeader moDataObject = new CaisseJsonHeader("CA", this.instanceID, receipt);
                String moData = new Gson().toJson(moDataObject);

                System.out.println("moData =" + moData);
                sendWebService("ticketReceiptToMO", "CA", "1", moData);
            }

            case "testSt": {
                ProductEntity productEntity = new ProductEntity("ref-01", 12);
                productService.createProduct(productEntity);
            }

            case "testdb": {
                System.out.println("inside testdb");

                ProductEntity entity = new ProductEntity();
                entity.setRef("ref-01");
                entity.setPrice(12);

                System.out.println("entity price = " + entity.getPrice());
                System.out.println(entity.toString());

                productService.createProduct(entity);
                return "OK";
            }
            case "testdb2": {
                System.out.println("inside testdb2");

                ProductEntity entity = new ProductEntity();
                entity.setRef("ref-02");
                entity.setPrice(12);

                productService.createProduct(entity);

                System.out.println(entity.toString());
                return "OK";
            }
            default: {
                System.out.println("Unknown function: " + fct);
                break;
            }
        }
        return "";
    }

    private void sendMsg(String targetFunction, String targetName, String targetInstance, String newData) {
        try {
            String targetUrl = "http://" + this.stip + "/api/msg?fct=" + targetFunction + "&target="
                    + targetName +"&targetInstance=" + targetInstance + "";
            System.out.println("targetUrl = " + targetUrl);

            MyHttpPost myHttpPost = new MyHttpPost(new HttpPost(targetUrl), new StringEntity("data=" + newData));
            myHttpPost.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String sendWebService(String targetFunction, String targetName, String targetInstance, String newData) {
        try {
            String url = "http://" + this.stip + "/api/service?fct=" + targetFunction + "&target=" + targetName
                    + "&targetInstance=" + targetInstance;

            System.out.println("targetUrl = " + url);
            MyHttpPost myHttpPost = new MyHttpPost(new HttpPost(url), new StringEntity("data=" + newData));
            return myHttpPost.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "KO";
    }
}
